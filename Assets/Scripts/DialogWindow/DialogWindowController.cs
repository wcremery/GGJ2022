using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DialogWindowController : MonoBehaviour
{
    [SerializeField]
    private GameObject[] items;
    private Button yesButton;
    private Button noButton;
    private TextMeshProUGUI textSpeech;
    private TextMeshProUGUI textInteraction;

    private Transform itemTransform;
    private ItemInventory itemInventory;

    private PlayerController player;

    private bool bInteraction;

    private LoadText loadText;

    private void Start()
    {
        GetRefs();

        transform.Find("Speech").gameObject.SetActive(false);
        transform.Find("Interaction").gameObject.SetActive(false);
    }

    private void GetRefs()
    {
        textSpeech = transform.Find("Speech/Description").GetComponent<TextMeshProUGUI>();
        textInteraction = transform.Find("Interaction/Description").GetComponent<TextMeshProUGUI>();
        
        itemInventory = FindObjectOfType<ItemInventory>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

        loadText = transform.Find("Speech/Description").GetComponent<LoadText>();
    }

    public void GetItem(Transform item)
    {
        itemTransform = item;
        
        if (itemTransform.GetComponent<LootItem>().GetIsCollectible())
        {
            yesButton = transform.Find("Interaction/Buttons/ButtonYes").GetComponent<Button>();
            noButton = transform.Find("Interaction/Buttons/ButtonNo").GetComponent<Button>();
            
            yesButton.onClick.AddListener(()=>PickUpItem());
            noButton.onClick.AddListener(()=>CloseDialogWindow());
        }
    }
    
    public void DisplayDialogWindow()
    {
        if (itemTransform.GetComponent<LootItem>().GetIsCollectible())
        {
            transform.Find("Interaction").gameObject.SetActive(true);
        }
        else
        {
            transform.Find("Speech").gameObject.SetActive(true);
        }

        player.SetCanMove(false);
        foreach (GameObject item in items)
        {
            item.GetComponent<ClickInteraction>().SetCanInteract(false);
        }
    }

    public void CloseDialogWindow()
    {
        if (transform.Find("Speech").gameObject.activeInHierarchy)
        {
            transform.Find("Speech").gameObject.SetActive(false);
        }
        else if (transform.Find("Interaction").gameObject.activeInHierarchy)
        {
            transform.Find("Interaction").gameObject.SetActive(false);
        }

        player.SetCanMove(true);
        foreach (GameObject item in items)
        {
            item.GetComponent<ClickInteraction>().SetCanInteract(true);
        }
    }

    public void FillText()
    {
        if (itemTransform.GetComponent<LootItem>().GetIsCollectible())
        {
            textInteraction.gameObject.SetActive(true);
            textInteraction.text =
                Resources.Load<ItemStats>("Items/" + itemTransform.GetComponent<LootItem>().GetItemId())
                    .GetItemDescription();
        }
        else
        {
            textSpeech.gameObject.SetActive(true);
            textSpeech.text =
                Resources.Load<ItemStats>("Items/" + itemTransform.GetComponent<LootItem>().GetItemId())
                    .GetItemDescription();
            loadText.SetText(Resources.Load<ItemStats>("Items/" + itemTransform.GetComponent<LootItem>().GetItemId()).GetItemsDescription());
        }
    }
    
    public void PickUpItem()
    {
        Debug.Log("Player added item into his inventory !");
        itemTransform.GetComponent<LootItem>().OnClickInteract();
        itemInventory.GetComponent<ItemInventory>().OnItemTake();
        CloseDialogWindow();
    }
    
}
