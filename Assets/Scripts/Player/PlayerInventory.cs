using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerInventory : MonoBehaviour
{
    [SerializeField]
    private List<Button> buttons;
    [SerializeField]
    private List<GameObject> windows;
    [SerializeField]
    private bool InventoryState = true;
    private bool activeInventory = false;

    [SerializeField, Header("Items part")]
    private List<GameObject> itemButtons;
    [SerializeField]
    private Transform itemButtonParent;
    [SerializeField]
    private GameObject itemButtonPref;

    private void Start()
    {
        windowsState();
        foreach (Button button in buttons)
        {
            button.onClick.AddListener(() => windowsState()); 
            button.onClick.AddListener(() =>  windowsState(buttons.IndexOf(button)));
        }
        buttons[0].gameObject.SetActive(false);
    }

    private void Update()
    {
        if (InventoryState && Input.GetButtonDown("Inventory"))
        {
            activeInventory = !activeInventory;
            if (activeInventory)
                windows[0].SetActive(true);
            else
            {
                windowsState();
            }
        }
    }

    public void windowsState(int activeWindow)
    {
        windows[activeWindow].SetActive(true);
        if (activeWindow != 0)
        {
            buttons[0].gameObject.SetActive(true);
        }
        switch (activeWindow)
        {
            case 1:
                OnItemWindowOpened();
                return;
        }
    }
    public void windowsState()
    {
        foreach (GameObject window in windows)
        {
            window.SetActive(false);
            buttons[0].gameObject.SetActive(false);
        }
    }

    //Items Part
    public void OnItemWindowOpened()
    {
        ItemStats[] items = Resources.LoadAll<ItemStats>("Scriptables/Items");
        foreach(ItemStats item in items)
        {
            if (item.GetNbInInventory() > 0)
            {
                bool buttonExist = false;
                foreach (GameObject itemButton in itemButtons)
                {
                    if (itemButton.name == item.GetItemName())
                    {
                        itemButton.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text
                            = "" + item.GetNbInInventory();
                        buttonExist = true;
                    }
                }
                if (!buttonExist)
                {
                    GameObject buttonPref = Instantiate(itemButtonPref, itemButtonParent);
                    buttonPref.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text
                            = "" + item.GetNbInInventory();
                    buttonPref.name = item.GetItemName();
                itemButtons.Add(buttonPref);
                }
            }
        }
    }
}
