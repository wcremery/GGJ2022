using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ScreenFade : MonoBehaviour
{
    [SerializeField]
    private UnityEvent startFadeEvent, endFadeEvent;
    [SerializeField]
    private bool activeFade = true, activeDelai = true, fadeIn = false;
    [SerializeField]
    private float fadeSpeed;
    private float delai = 0;
    private bool fadeIsLaunch = false;
    private float timer;

    public void Start()
    {
        startFadeEvent.Invoke();
        if (transform.GetChild(0).GetComponent<Image>().color.a == 0)
        {
            transform.GetChild(0).GetComponent<Image>().enabled = false;
        }
    }
    public void LaunchFadeAfterDelai(int delai)
    {
        transform.GetChild(0).GetComponent<Image>().enabled = true;
        if (fadeIn)
        {
            timer = 0;
            transform.GetChild(0).GetComponent<Image>().color = new Color(0, 0, 0, 0);
        }
        else
        {
            timer = 1;
            transform.GetChild(0).GetComponent<Image>().color = new Color(0, 0, 0, 1);
        }
        if (activeDelai)
            this.delai = delai;
        if (activeFade)
            StartCoroutine(LaunchFade());
        else
        {
            transform.GetChild(0).GetComponent<Image>().color = new Color(0, 0, 0, 0);
        }
    }
    private IEnumerator LaunchFade()
    {
        yield return new WaitForSeconds(delai);
        fadeIsLaunch = true;
    }
    private void Update()
    {
        if (fadeIsLaunch)
        {
            if (fadeIn)
            {
                if (timer < 1)
                {
                    timer += fadeSpeed * Time.deltaTime;
                    transform.GetChild(0).GetComponent<Image>().color = new Color(0, 0, 0, timer);
                }
                else
                {
                    PlayerPrefs.GetFloat("transitionScreen", 1);
                    endFadeEvent.Invoke();
                    fadeIsLaunch = false;
                }
            }
            else
            {
                if (timer > 0)
                {
                    timer -= fadeSpeed * Time.deltaTime;
                    transform.GetChild(0).GetComponent<Image>().color = new Color(0, 0, 0, timer);
                }
                else
                {
                    transform.GetChild(0).GetComponent<Image>().enabled = false;
                    PlayerPrefs.GetFloat("transitionScreen", 0);
                    endFadeEvent.Invoke();
                    fadeIsLaunch = false;
                }
            }
        }
    }
    public void SetFadeDirection(bool fadeIn)
    {
        this.fadeIn = fadeIn;
    }
    public void SetFadeSpeed(float speed)
    {
        this.fadeSpeed = speed;
    }
}
