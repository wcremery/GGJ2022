using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(TextMeshProUGUI))]
public class LoadText : MonoBehaviour
{
    [System.Serializable]
    public class TextAndSpeed
    {
        public TextAndSpeed(string sentence, float sentenceSpeed)
        {
            this.sentence = sentence;
            this.sentenceSpeed = sentenceSpeed;
        }

        [SerializeField, TextArea(1, 3)]
        public string sentence;
        [SerializeField]
        public float sentenceSpeed;
    }
    
    [SerializeField]
    private TextAndSpeed[] textAndSpeeds;
    [SerializeField]
    private float timeBetweenSentences;
    [SerializeField]
    private UnityEvent waitClickEvent, clickToChangeTextEvent, endEvent;
    [SerializeField]
    private bool setTextOnStart = true, clickToChangeActualText = false;

    [SerializeField] private TextMeshProUGUI uiText;
    private float showSpeed = 0.05f, dialogSpeed;
    private string showText, completeText;
    private bool coroutineProtect, loadText, waitForClick = false, loopIsbreak = false;
    private int actualText = 0, enter = 0;

    private void Start()
    {
        if (dialogSpeed == 0)
        {
            dialogSpeed = PlayerPrefs.GetFloat("TextSpeedSlider") != 0 ? PlayerPrefs.GetFloat("TextSpeedSlider") : 0.05f;
        }
        if (setTextOnStart)
        {
            ShowActuAlText();
        }
    }

    private void Update()
    {
        dialogSpeed = PlayerPrefs.GetFloat("TextSpeedSlider");
        if (loadText && !coroutineProtect)
        {
            StartCoroutine(LoadLetters());
            coroutineProtect = true;
        }

        else if (loadText && coroutineProtect) { uiText.text = showText; }
    }

    private void ShowActuAlText()
    {
        completeText = textAndSpeeds[actualText].sentence;
        if (textAndSpeeds[actualText].sentenceSpeed == 0)
            showSpeed = dialogSpeed;
        else
            showSpeed = textAndSpeeds[actualText].sentenceSpeed;
        uiText.text = null;
        showText = null;
        loadText = true;
        coroutineProtect = false;
    }
    private IEnumerator LoadLetters()
    {
        enter++;
        int textSize = 0;
        while ((completeText != null && textSize < completeText.Length) && enter < 2)
        {
            showText += completeText[textSize++];
            yield return new WaitForSeconds(showSpeed);
        }
        enter--;
        coroutineProtect = false;
        loadText = false;
        if (!clickToChangeActualText)
            StartCoroutine(GoToNextText());
        else
        {
            waitForClick = true;
            waitClickEvent.Invoke();
        }
    }

    public void WhenClick()
    {
        if (waitForClick)
        {
            clickToChangeTextEvent.Invoke();
            StartCoroutine(GoToNextText());
            waitForClick = false;
        }
    }

    private IEnumerator GoToNextText()
    {
        yield return new WaitForSeconds(timeBetweenSentences);
        actualText++;
        if (actualText < textAndSpeeds.Length)
        {
            ShowActuAlText();
        }
        else EventOnEnd();
    }


    public void EventOnEnd()
    {
        endEvent.Invoke();
    }
    public void SetText (string[] texts)
    {
            dialogSpeed = PlayerPrefs.GetFloat("TextSpeedSlider");
            textAndSpeeds = new TextAndSpeed[texts.Length];

            for (int i = 0; i < texts.Length; i++)
            {
                textAndSpeeds[i] = new TextAndSpeed(texts[i], dialogSpeed);
            }
            completeText = null;
            actualText = 0;
            ShowActuAlText();
    }
}
