using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Space(3), Header("Caracter Section")]
    private Animator anim;
    [SerializeField]
    private float JumpHeight = 2;
    [SerializeField]
    private float speed;
    [SerializeField]
    private float gravity = -9.81f;
    private Rigidbody2D rb2D;
    [SerializeField]
    private bool canMove = true, canJump = false;
    [SerializeField]
    private AudioSource walkSource;
    [SerializeField]
    private AudioClip[] walkClip;
    [SerializeField]
    private float soundSpeed = 1;

    [SerializeField, Space(3), Header("Ground detect Section")]
    private Transform groundChecker;
    [SerializeField]
    private LayerMask groundLayer;
    [SerializeField]
    private float groundDistance = 0.4f;
    [SerializeField]
    private float stepSmooth = 2f, stepRayDistance = 1.5f;
    [SerializeField] 
    private GameObject stepRayUpper;
    [SerializeField]
    private LayerMask layerMask;

    private bool isGrounded;
    private Vector3 velocity;

    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (canMove)
        {
            RaycastHit2D hitUpper = Physics2D.Raycast(stepRayUpper.transform.position, Vector2.right * Input.GetAxis("Horizontal"), stepRayDistance, layerMask);
            Debug.DrawRay(stepRayUpper.transform.position, Vector2.right* stepRayDistance * Input.GetAxis("Horizontal"), Color.green);
            if (hitUpper.collider != null)
            {
                Debug.Log(hitUpper.collider);
                rb2D.position += new Vector2(0f, stepSmooth * Time.deltaTime);
            }
            isGrounded = Physics2D.OverlapCircle(groundChecker.position, groundDistance, groundLayer);
            if (isGrounded)
            {
                if (velocity.y < 0)
                {
                    velocity.y = -2f;
                }
                if (Input.GetButtonDown("Jump") && canJump)
                {
                    velocity.y = Mathf.Sqrt(JumpHeight * speed * -2f * gravity);
                }
            }
            velocity.y += gravity * Time.deltaTime;

            velocity.x = speed * Input.GetAxis("Horizontal");
            
            if (Input.GetAxis("Horizontal") != 0) 
            GetComponent<SpriteRenderer>().flipX = Input.GetAxis("Horizontal") < 0 ? true : false;
            rb2D.MovePosition(new Vector2(rb2D.position.x + velocity.x * Time.fixedDeltaTime, rb2D.position.y + velocity.y * Time.fixedDeltaTime));
        }
        if (velocity.x == 0 || !canMove)
        {
            //idle animation
            anim.SetBool("isBored", true);
            anim.SetBool("isMoving", false);
        }
        else if (canMove && velocity.x != 0)
        {
            //walk animation
            anim.SetBool("isMoving", true);
            anim.SetBool("isBored", false);
            if (!walkSource.isPlaying)
            {
                walkSource.clip = walkClip[Random.Range(0, 2)];
                walkSource.pitch = soundSpeed;
                walkSource.Play();
            }
        }
    }
    public void SetCanMove(bool canMove)
    {
        this.canMove = canMove;
    }
}
