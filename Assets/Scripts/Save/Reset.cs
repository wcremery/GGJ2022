using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reset : MonoBehaviour
{
    public void ResetGame()
    {
        PlayerPrefs.SetString("Scene", "");
        PlayerPrefs.SetFloat("playerX", 0);
        PlayerPrefs.SetFloat("playerY", 0);
        PlayerPrefs.SetFloat("playerZ", 0);
        ItemStats[] itemsInInventory = Resources.LoadAll<ItemStats>("Items/");
        foreach (ItemStats item in itemsInInventory)
        {
            PlayerPrefs.SetInt("Item" + item.GetId(), 0);
            item.SetNbInInventory(0);
        }
    }
}
