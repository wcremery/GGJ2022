using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Load : MonoBehaviour
{
    private GameObject player;
    private ItemInventory itemInventory;
    void Start()
    {
        if (PlayerPrefs.GetInt("Load") == 1)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            player.transform.GetChild(1).transform.GetChild(3).transform.GetChild(0).GetComponent<Image>().color =
                new Color(0,0,0,PlayerPrefs.GetFloat("transitionScreen"));
            player.transform.position = new Vector3(PlayerPrefs.GetFloat("playerX"), PlayerPrefs.GetFloat("playerY"), PlayerPrefs.GetFloat("playerZ"));
            ItemStats[] itemsInInventory = Resources.LoadAll<ItemStats>("Items/");
            foreach (ItemStats item in itemsInInventory)
            {
                item.SetNbInInventory(PlayerPrefs.GetInt("Item" + item.GetId()));
                PlayerPrefs.SetInt("Item" + item.GetId(), item.GetNbInInventory());
            }
            PlayerPrefs.SetInt("Load", 0);
            itemInventory = FindObjectOfType<ItemInventory>();
            itemInventory.GetComponent<ItemInventory>().OnItemTake();
        }
    }
}
