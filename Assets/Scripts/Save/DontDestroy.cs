using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DontDestroy : MonoBehaviour
{
    [SerializeField, Header("Sestroy if scene is ")]
    private string sceneName = "TitleScreen";
    private void Awake()
    {
        if (SceneManager.GetActiveScene().name != sceneName)
            DontDestroyOnLoad(this.gameObject);
    }
}
