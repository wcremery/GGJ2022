using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Save : MonoBehaviour
{
    private GameObject player;

    void Update()
    {
        if (player == null)
            player = GameObject.FindGameObjectWithTag("Player");
    }
    public void SaveOnGoToMenu()
    {
        SaveGame();
    }
    private void OnApplicationQuit()
    {
        SaveGame();
    }

    private void SaveGame()
    {
        PlayerPrefs.SetString("Scene", SceneManager.GetActiveScene().name);
        PlayerPrefs.SetFloat("playerX", player.transform.position.x);
        PlayerPrefs.SetFloat("playerY", player.transform.position.y);
        PlayerPrefs.SetFloat("playerZ", player.transform.position.z);
        ItemStats[] itemsInInventory = Resources.LoadAll<ItemStats>("Items/");
        foreach (ItemStats item in itemsInInventory)
        {
            PlayerPrefs.SetInt("Item" + item.GetId(), item.GetNbInInventory());
        }
    }
}
