using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadPlayerPrefs : MonoBehaviour
{
    void Start()
    {
        List<ItemStats> itemsToLoad = new List<ItemStats>(Resources.LoadAll<ItemStats>("Scriptables/Items"));
        foreach (ItemStats item in itemsToLoad)
        {
            item.SetNbInInventory(PlayerPrefs.GetInt("Item" + itemsToLoad.IndexOf(item)));
        }
    }
}
