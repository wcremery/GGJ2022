using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditDefile : MonoBehaviour
{
    [SerializeField]
    private float startPosY, maxPos, speed;
    private void OnEnable()
    {
        transform.localPosition = new Vector3(0, startPosY, 0);
    }
    private void Update()
    {
        if (transform.localPosition.y <= maxPos)
        {
            transform.localPosition -= new Vector3(0, -speed * Time.deltaTime, 0);
        }
    }
}
