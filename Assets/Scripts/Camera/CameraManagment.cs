using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManagment : MonoBehaviour
{
    [SerializeField]
    private Transform player;
    [SerializeField]
    private float minPos, maxPos;
    private void Update()
    {
        if (transform.position.x <= minPos || transform.position.x > maxPos)
        {
            FollowPlayer(false);
        }
        if (player.position.x > minPos && player.position.x < maxPos)
        {
            transform.position = new Vector3(player.position.x, 4.5f, -10);
            FollowPlayer(true);
        }
    }
    public void FollowPlayer(bool active)
    {
        Vector2 pos = transform.position;
        if (pos.x == minPos)
            transform.position = new Vector3(minPos+0.1f, transform.position.y, -10);
        if (pos.x == maxPos)
            transform.position = new Vector3(maxPos-0.1f, transform.position.y, -10);
    }
}
