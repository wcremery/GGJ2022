using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    [SerializeField]
    private AudioSource environmentNoise, environmentMusic;
    [SerializeField]
    private List<AudioSource> otherSoundsSources;
    [SerializeField]
    private float[] initMVolumes, initBMVolumes, initSVolumes;
    [SerializeField]
    private AudioClip[] musics, bmusics, sounds;
    [SerializeField]
    private Slider generalSlider, musicSlider, soundSlider;
    void Start()
    {
        generalSlider.value = PlayerPrefs.GetFloat("generalVolume");
        musicSlider.value = PlayerPrefs.GetFloat("musicVolume"); 
        soundSlider.value = PlayerPrefs.GetFloat("soundVolume");
        //ChangeSound(0);
        //ChangeMusic(0, 1);
        generalSlider.onValueChanged.AddListener(delegate { SaveVolume(); });
        musicSlider.onValueChanged.AddListener(delegate { SaveVolume(); });
        soundSlider.onValueChanged.AddListener(delegate { SaveVolume(); });
    }

    public void ChangeSound(int number)
    {
        environmentNoise.Stop();
        environmentNoise.clip = sounds[number];
        PlayerPrefs.SetInt("actualSound", number);
        Volume();
        environmentNoise.Play();
    }
    public void ChangeMusic(int number, int musicList)
    {
        environmentMusic.Stop();
        switch (musicList)
        {
            case 0:
                if (bmusics.Length - 1 >= number)
                {
                    environmentMusic.clip = bmusics[number];
                }
            break;
            case 1:
                if (musics.Length - 1 >= number)
                {
                    environmentMusic.clip = musics[number];
                }
            break;
        }
        PlayerPrefs.SetInt("actualMusiqueList", musicList);
        PlayerPrefs.SetInt("actualMusique", number);
        Volume();
        environmentMusic.Play();
    }
    public void SaveVolume()
    {
        PlayerPrefs.SetFloat("generalVolume", generalSlider.value);
        PlayerPrefs.SetFloat("soundVolume", soundSlider.value);
        PlayerPrefs.SetFloat("musicVolume", musicSlider.value);
        Volume();
    }
    public void Volume()
    {
        switch (PlayerPrefs.GetInt("actualMusiqueList"))
            {
            case 0:
                environmentMusic.volume = initBMVolumes[PlayerPrefs.GetInt("actualMusique")] * generalSlider.value * musicSlider.value;
                break;
            case 1:
                environmentMusic.volume = initMVolumes[PlayerPrefs.GetInt("actualMusique")] * generalSlider.value * musicSlider.value;
                break;
        }
        environmentNoise.volume = initSVolumes[PlayerPrefs.GetInt("actualSound")] * generalSlider.value * soundSlider.value;
        foreach (AudioSource source in otherSoundsSources)
        {
            if (source !)
            source.volume = 0.3f * generalSlider.value * soundSlider.value;
        }
    }
    public void SetAudioSource(AudioSource source)
    {
        otherSoundsSources.Add(source);
        SaveVolume();
    }
}
