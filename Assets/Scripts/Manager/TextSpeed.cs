using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextSpeed : MonoBehaviour
{
    [SerializeField]
    private LoadText testText;
    [SerializeField]
    private Slider textSpeedSlider;
    [SerializeField]
    private string[] testMessage;
    private float textSliderActualSpeed;
    void Start()
    {
        testMessage[0] = "Voici comment sera la vitesse des dialogues.";
        textSpeedSlider.value = PlayerPrefs.GetFloat("TextSpeedSlider") != 0 ? PlayerPrefs.GetFloat("TextSpeedSlider") : 0.05f;
        PlayerPrefs.SetFloat("TextSpeedSlider", textSpeedSlider.value);
        textSliderActualSpeed = textSpeedSlider.value;
    }

    private void clampSlider()
    {
        textSpeedSlider.value = Mathf.Clamp((float)System.Math.Round(textSpeedSlider.value, 2), 0.01f, 0.1f);
        PlayerPrefs.SetFloat("TextSpeedSlider", textSpeedSlider.value);
        testText.SetText(testMessage);
        textSpeedSlider.enabled = false;
    }
    private void Update()
    {
        if (Mathf.Clamp((float)System.Math.Round(textSpeedSlider.value, 2), 0.01f, 0.1f) != textSliderActualSpeed)
        {
            clampSlider();
            textSliderActualSpeed = textSpeedSlider.value;
        }
        textSpeedSlider.value = Mathf.Clamp((float)System.Math.Round(textSpeedSlider.value, 2), 0.01f, 0.1f);
    }
}
