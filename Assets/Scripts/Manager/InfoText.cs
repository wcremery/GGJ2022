using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InfoText : MonoBehaviour
{
    private float waitTime;
    void Start()
    {
        GetComponent<TextMeshProUGUI>().enabled = false;
    }

    private void Update()
    {
        if (waitTime > 0)
        {
            waitTime -= Time.deltaTime;
        }
        else
        {
            waitTime = 0;
            GetComponent<TextMeshProUGUI>().enabled = false;
        }
    }

    public void SetText(string text, int enabeledtime)
    {
        waitTime = enabeledtime;
        GetComponent<TextMeshProUGUI>().text = text;
        GetComponent<TextMeshProUGUI>().enabled = true;
    }
}
