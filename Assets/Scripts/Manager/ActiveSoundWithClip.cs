using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveSoundWithClip : MonoBehaviour
{
    [SerializeField]
    private AudioSource ItemSource;

    public void LaunchSound(AudioClip audioClip)
    {
        ItemSource.clip = audioClip;
        ItemSource.Play();
    }
}
