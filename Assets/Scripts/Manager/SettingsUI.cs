using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SettingsUI : MonoBehaviour
{
    [SerializeField]
    private GameObject settingsCanvas;
    [SerializeField]
    private List<Button> settingsButtons;
    [SerializeField]
    private List<GameObject> settingsWindows;
    [SerializeField]
    private GameObject menuButton;

    private bool settingsState = false;

    void Start()
    {
        if (SceneManager.GetActiveScene().name == "TitleScreen")
            menuButton.SetActive(false);
        else menuButton.SetActive(true);
        settingsCanvas.SetActive(false);
        foreach (Button butt in settingsButtons)
        {
            butt.onClick.AddListener(() =>
            {
                foreach (GameObject window in settingsWindows)
                {
                    window.SetActive(false);
                    if (settingsButtons.IndexOf(butt) == settingsWindows.IndexOf(window))
                    {
                        window.SetActive(true);
                    }
                }
            });
        }
    }

    public void Update()
    {
        if (Input.GetButtonDown("Settings"))
        {
            settingsState = !settingsState;
            settingsCanvas.SetActive(settingsState);
        }
    }

    private void OnDisable()
    {
        foreach (GameObject window in settingsWindows)
        {
            window.SetActive(false);
        }
    }
}
