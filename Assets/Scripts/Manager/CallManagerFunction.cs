using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallManagerFunction : MonoBehaviour
{
    private GameObject manager;
    [SerializeField, Header("To call functionOnStart"), Tooltip("Functions : changeAudio, changeSound, changeMusic, changeScene, addAudioSource")]
    private string functionName;
    [SerializeField]// AudioClip for changeAudio function
    private AudioClip clip;
    [SerializeField]// SceneString for changeMusic function
    private int musicOrAudioNum;
    [SerializeField]// SceneSource for setAudioSource function
    private AudioSource source;
    [SerializeField]// SceneString for changeScene function
    private string sceneName;
    [SerializeField]
    private bool activeOnStart = false;

    private void Start()
    {
        if(activeOnStart)
        {
            CallFunctionFromManager(functionName);
        }
    }
    public void SetAudioClip(AudioClip clip)
    {
        this.clip = clip;
    }
    public void SetSceneName(string sceneName)
    {
        this.sceneName = sceneName;
    }
    public void CallFunctionFromManager(string functionName)
    {
        manager = GameObject.Find("Manager");
        switch (functionName)
        {
            case "changeAudio":
                manager.GetComponent<ActiveSoundWithClip>().LaunchSound(clip);
                return;
            case "changeScene":
                manager.GetComponent<ChangeScene>().OnChangeScene(sceneName);
                return;
            case "addAudioSource":
                    manager.GetComponent<AudioManager>().SetAudioSource(source);
                return;
            case "changeMusic":
                manager.GetComponent<AudioManager>().ChangeMusic(musicOrAudioNum, 1);
                return;
            case "changeSound":
                manager.GetComponent<AudioManager>().ChangeSound(musicOrAudioNum);
                Debug.Log("changeSound " + musicOrAudioNum);
                return;
        }
    }
    public void ManagerAddAudioSource(AudioSource source)
    {
        manager.GetComponent<AudioManager>().SetAudioSource(source);
    }
    public void ManagerChangeAudio(AudioClip clip)
    {
        manager.GetComponent<ActiveSoundWithClip>().LaunchSound(clip);
    }
    public void ManagerChangeMusic(int musicNum)
    {
        manager.GetComponent<AudioManager>().ChangeMusic(musicNum, 1);
    }
    public void ManagerChangeSound(int soundNum)
    {
        manager.GetComponent<AudioManager>().ChangeSound(musicOrAudioNum);
    }
    public void ManagerChangeScene(string sceneName)
    {
        manager.GetComponent<ChangeScene>().OnChangeScene(sceneName);
    }
}
