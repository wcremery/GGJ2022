using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour
{
    private Button m_newGameButton;
    private Button m_playButton;
    private Button m_optionsButton;
    private Button m_creditsButton;
    private Button m_quitButton;

    [SerializeField] private string startScene;
    private GameObject OptionsPanel;
    [SerializeField] private GameObject CreditsPanel;
    [SerializeField] private GameObject[] buttons;
    [SerializeField] private CallManagerFunction callChangeSceneScript;

    private void Awake()
    {
        GetButtonsRefs();
        m_newGameButton.onClick.AddListener(() => ResetGame());
        m_playButton.onClick.AddListener(()=>LoadGameScreen());
        m_optionsButton.onClick.AddListener(()=>DisplayOptions());
        m_creditsButton.onClick.AddListener(()=>DisplayCredits());
        m_quitButton.onClick.AddListener(()=>QuitGame());
        PlayerPrefs.SetInt("Load", 0); 
        if (PlayerPrefs.GetString("Scene").Length <= 0)
        {
            m_playButton.interactable = false;
        }
    }

    void GetButtonsRefs()
    {
        OptionsPanel = GameObject.Find("Manager").transform.GetChild(1).gameObject;
        m_newGameButton = buttons[0].GetComponent<Button>();
        m_playButton = buttons[1].GetComponent<Button>();
        m_optionsButton = buttons[2].GetComponent<Button>();
        m_creditsButton = buttons[3].GetComponent<Button>();
        m_quitButton = buttons[4].GetComponent<Button>();
    }
    void ResetGame()
    {
        GetComponent<Reset>().ResetGame();
        callChangeSceneScript.SetSceneName(startScene);
        callChangeSceneScript.gameObject.GetComponent<ScreenFade>().SetFadeDirection(true);
        callChangeSceneScript.gameObject.GetComponent<ScreenFade>().LaunchFadeAfterDelai(0);
    }
    void LoadGameScreen()
    {
        if (PlayerPrefs.GetString("Scene").Length > 0)
        {
            PlayerPrefs.SetInt("Load", 1);
            callChangeSceneScript.SetSceneName(PlayerPrefs.GetString("Scene"));
            callChangeSceneScript.gameObject.GetComponent<ScreenFade>().SetFadeDirection(true);
            callChangeSceneScript.gameObject.GetComponent<ScreenFade>().LaunchFadeAfterDelai(0);
        }
        else
        {
            m_playButton.interactable = false;
            Debug.LogError("Sauvegarde non trouv�e, cliquez sur nouvelle partie !");
        }
    }

    void DisplayOptions()
    {
        OptionsPanel.SetActive(true);
        Debug.Log("Display options panel");
    }

    void DisplayCredits()
    {
        CreditsPanel.SetActive(true);
    }

    void QuitGame()
    {
        Debug.Log("Thank you for playing");
        Application.Quit();
    }
}
