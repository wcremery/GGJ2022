using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Sciptables/Items")]
public class ItemStats : ScriptableObject
{
    [SerializeField] private int id, nbInInventory;

    [SerializeField] private string itemName, itemDestination;
    [SerializeField] private string[] itemDescription;

    [SerializeField] private bool bCollectible;

    [SerializeField]
    private Sprite[] sprites;
    
    public int GetId()
    {
        return id;
    }
    public int GetNbInInventory()
    {
        return nbInInventory;
    }
    public void SetNbInInventory(int nbInInventory)
    {
        this.nbInInventory = nbInInventory;
    }
    public string GetItemName()
    {
        return itemName;
    }
    public string GetItemDestination()
    {
        return itemDestination;
    }

    public string GetItemDescription()
    {
        return itemDescription[0];
    }
    
    public string[] GetItemsDescription()
    {
        return itemDescription;
    }

    public Sprite[] GetSpriteSheet()
    {
        return sprites;
    }

    public bool GetIsCollectible()
    {
        return bCollectible;
    }
}
