using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Interaction : MonoBehaviour
{
    [SerializeField] 
    private bool actionToInteract;
    private bool interactState = false, collide = false;
    [SerializeField]
    private UnityEvent collideEvent;

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
            collide = true;
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
            collide = false;
    }
    private void Update()
    {
        if (collide && (!actionToInteract || (actionToInteract && Input.GetButtonDown("Interact"))))
        {
            Debug.Log("interact");
            interactState = true;
            collideEvent.Invoke();
        }
        else
            interactState = false;
    }

    public bool GetInteractState()
    {
        return interactState;
    }
}
