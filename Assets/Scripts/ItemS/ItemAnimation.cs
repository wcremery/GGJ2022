using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class ItemAnimation : MonoBehaviour
{
    private float m_speedAnimation = 80f;
    private Sprite[] m_itemSprites;

    private int m_animIndex = 0;
    
    private SpriteRenderer m_itemCurrentSprite;

    // Start is called before the first frame update
    void Start()
    {
        GetRefs();
        Init();
        if (m_itemSprites.Length > 1) StartCoroutine(PlayItemAnimation());
    }

    private void Init()
    {
        gameObject.SetActive(true);
    }

    private void GetRefs()
    {
        m_itemCurrentSprite = GetComponent<SpriteRenderer>();
        m_itemSprites = Resources.Load<ItemStats>("Items/" + GetComponent<LootItem>().GetItemId()).GetSpriteSheet();
    }

    private IEnumerator PlayItemAnimation()
    {
        m_itemCurrentSprite.sprite = m_itemSprites[m_animIndex];
        m_animIndex = (m_animIndex + 1) % m_itemSprites.Length;
        yield return new WaitForSeconds(m_speedAnimation * Time.deltaTime);
        yield return 0;

        StartCoroutine(PlayItemAnimation());
    }
}