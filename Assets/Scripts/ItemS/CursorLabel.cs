using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CursorLabel : MonoBehaviour
{
    void Start()
    {
        
    }
    void Update()
    {
        transform.position = Input.mousePosition;
    }
    public void SetText(int id)
    {
        transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = Resources.Load<ItemStats>("Items/" + id).GetItemName();
    }
}
