using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ButtonInteraction : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    [SerializeField]
    private UnityEvent clickEvent, hoverEvent, exitEvent;

    private Button button;
    private void Start()
    {
        button = GetComponent<Button>();
    }
    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        if (button.interactable)
            hoverEvent.Invoke();
    }

    public void OnPointerExit(PointerEventData pointerEventData)
    {
        if (button.interactable)
            exitEvent.Invoke();
    }
    public void OnPointerClick(PointerEventData pointerEventData)
    {
        if (button.interactable)
            clickEvent.Invoke();
    }
}
