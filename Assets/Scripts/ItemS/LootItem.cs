using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootItem : MonoBehaviour
{
    [SerializeField]
    private int itemId;
    [SerializeField]
    private bool respawnItem = false;
    [SerializeField]
    private int respawnTime = 30;
    private bool interactable = true, interaction = false;
    [SerializeField] private bool bCollectible = false;
    
    private void Awake()
    {
        ItemStats item = Resources.Load<ItemStats>("Items/" + itemId);
        if (item.GetNbInInventory() > 0)
        {
            SetObjectInteractable(false);
        }
    }
    void Update()
    {
        if (GetComponent<Interaction>() != null && GetComponent<ClickInteraction>() == null)
        {
            bool interaction = GetComponent<Interaction>().GetInteractState();
        }
    }

    private IEnumerator RespawnItem()
    {
        yield return new WaitForSeconds(respawnTime);
        SetObjectInteractable(true);
    }

    private void SetObjectInteractable(bool interactable)
    {
        gameObject.GetComponent<SpriteRenderer>().enabled = interactable;
        gameObject.GetComponent<Collider2D>().enabled = interactable;
        this.interactable = interactable;
    }
    public void Loot()
    {
        ItemStats item = Resources.Load<ItemStats>("Items/" + itemId);
        item.SetNbInInventory(item.GetNbInInventory() + 1);
        SetObjectInteractable(false);
        if (respawnItem)
            StartCoroutine(RespawnItem());
    }
    public void OnClickInteract()
    {
        interaction = true;
        Loot();
    }

    public int GetItemId()
    {
        return itemId;
    }

    public bool GetIsCollectible()
    {
        return bCollectible;
    }
}
