using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHover : MonoBehaviour
{
    [SerializeField]
    private List<Transform> outlines;
    [SerializeField, Header("hover section")]
    private float outLineScale = 1.1f;
    [SerializeField]
    private Material hoverMaterial;
    [SerializeField]
    private Color hoverColor = new Color(0,0,0,1);

    // private SpriteRenderer spriteRenderer;
    // private SpriteRenderer spriteOutline;
    // public void CreateHover()
    // {
    //     spriteRenderer = GetComponent<SpriteRenderer>();
    //     spriteOutline = spriteRenderer;
    //     spriteOutline.color = Color.black;
    //     spriteOutline.transform.localScale
    // }
    
    public void ActiveTransformHover()
    {
        if (outlines.Count == 0)
        {
            Transform outlineBase = Instantiate(transform, transform.position + new Vector3(outLineScale,0,0), transform.rotation,transform);
            
            foreach (Component component in outlineBase.GetComponents(typeof(Component)))
            {
                if (component.GetType() != typeof(Transform) && component.GetType() != typeof(SpriteRenderer))
                {
                    Destroy(component);
                }
            }
            outlineBase.GetComponent<SpriteRenderer>().material = hoverMaterial;
            outlineBase.GetComponent<SpriteRenderer>().sortingOrder -= 1;
            outlineBase.GetComponent<SpriteRenderer>().color = hoverColor;
            outlines.Add(outlineBase);
            outlines.Add(Instantiate(outlines[0], transform.position + new Vector3(-outLineScale, 0, 0), transform.rotation, transform));
            outlines.Add(Instantiate(outlines[0], transform.position + new Vector3(0, outLineScale, 0), transform.rotation, transform));
            outlines.Add(Instantiate(outlines[0], transform.position + new Vector3(0, -outLineScale, 0), transform.rotation, transform));
            outlines.Add(Instantiate(outlines[0], transform.position + new Vector3(outLineScale, outLineScale, 0), transform.rotation, transform));
            outlines.Add(Instantiate(outlines[0], transform.position + new Vector3(outLineScale, -outLineScale, 0), transform.rotation, transform));
            outlines.Add(Instantiate(outlines[0], transform.position + new Vector3(-outLineScale, outLineScale, 0), transform.rotation, transform));
            outlines.Add(Instantiate(outlines[0], transform.position + new Vector3(-outLineScale, -outLineScale, 0), transform.rotation, transform));
            foreach (Transform outline in outlines)
            {
                outline.localScale = new Vector3(1, 1, 1);
            }
        }
        else
            foreach (Transform outline in outlines)
            {
                outline.gameObject.SetActive(true);
                outline.GetComponent<SpriteRenderer>().enabled = true;
            }
    }

    public void DesactiveTransformHover()
    {
        if (outlines.Count > 0)
            foreach (Transform outline in outlines) outline.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (outlines.Count > 0)
        {
            foreach (Transform outline in outlines)
            {
                //outline.localScale = new Vector3(outLineScale, outLineScale, 0);
                outline.GetComponent<SpriteRenderer>().color = hoverColor;
            }
        }
    }
}
