using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class ItemInventory : MonoBehaviour
{
    [SerializeField, Header("Items Inventory Section")]
    private GameObject inventoryWindow;
    [SerializeField]
    private List<GameObject> itemButtons;
    [SerializeField]
    private float inventorySpeedApear;
    [SerializeField]
    private Vector3 hidePos;

    private bool activeInventory = false;
    private Vector2 startPos;
    private void Start()
    {
        startPos = inventoryWindow.transform.position;
        hidePos = transform.position + hidePos;
        OnItemTake();
    }

    public void OnItemTake()
    {
        ItemStats[] items = Resources.LoadAll<ItemStats>("Items/");
        foreach (ItemStats item in items)
        {
            if (item.GetNbInInventory() > 0)
            {
                Debug.Log("ici");
                int buttonToChange = 0;
                if (item.GetItemDestination() == "Sister")
                    buttonToChange = 0;
                if (item.GetItemDestination() == "Mother")
                    buttonToChange = 1;
                    itemButtons[buttonToChange].GetComponent<Image>().sprite = Resources.Load<Sprite>("Items/" + item.GetId());
                    itemButtons[buttonToChange].name = item.GetItemName();
            }
        }
        foreach (GameObject button in itemButtons)
        {
            if (button.name != "Button")
                activeInventory = true;
        }
        if (!activeInventory)
        {
            inventoryWindow.transform.position = hidePos;
        }
    }
    private void Update()
    {
        Vector2 pos = inventoryWindow.transform.position;
        if (activeInventory && pos.x < startPos.x)
        {
            pos.x += inventorySpeedApear * Time.deltaTime;
            inventoryWindow.transform.position = pos;
        }
    }
}
