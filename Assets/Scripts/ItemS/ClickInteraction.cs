using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ClickInteraction : MonoBehaviour
{
    [SerializeField]
    private UnityEvent clickEvent, hoverEvent, exitEvent;
    [SerializeField]
    private bool isInteractable = true;
    
    private void OnMouseUpAsButton()
    {
        if (isInteractable)
        {
            clickEvent.Invoke();
        }
    }
    private void OnMouseOver()
    {
        if (isInteractable)
        {
            Debug.Log("mouseOver");
            hoverEvent.Invoke();
        }
    }
    void OnMouseExit()
    {
        exitEvent.Invoke();
    }

    public void SetCanInteract(bool interact)
    {
        isInteractable = interact;
        if (interact == false)
            OnMouseExit();
    }
}
