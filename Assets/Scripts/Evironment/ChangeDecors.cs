using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeDecors : MonoBehaviour
{
    [SerializeField]
    private int objectsNumInScene;
    [SerializeField]
    private bool[] objectsInteracted;
    [SerializeField]
    private Sprite decorsHappySprite;
    void Start()
    {
        objectsInteracted = new bool[objectsNumInScene];
    }

    public void OnInteractActiveInteraction(int objectNumInScene)
    {
        objectsInteracted[objectNumInScene] = true;
        int objectFalse = 0;
        foreach (bool objectInteracted in objectsInteracted)
        {
            if (objectInteracted == false)
            {
                objectFalse++;
            }
        }
        if (objectFalse == 0)
        {
            transform.GetComponent<SpriteRenderer>().sprite = decorsHappySprite;
        }
    }
}
