using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelsController : MonoBehaviour
{

    private GameObject displayOptionsGameObject;
    private GameObject displayCreditsGameObject;

    void ClosePanel()
    {
        if (displayCreditsGameObject.activeSelf)
        {
            displayCreditsGameObject.SetActive(false);
        }
        else if (displayOptionsGameObject.activeSelf)
        {
            displayOptionsGameObject.SetActive(false);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        GetRefs();
        
        displayOptionsGameObject.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(()=>ClosePanel());
        displayCreditsGameObject.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(()=>ClosePanel());
        
        displayCreditsGameObject.SetActive(false);
    }

    void GetRefs()
    {
        displayOptionsGameObject = FindObjectOfType<SettingsUI>().transform.GetChild(1).gameObject;
        displayCreditsGameObject = transform.Find("CreditsPanel").gameObject;
    }
}
